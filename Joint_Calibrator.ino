#include "./readPinFast.h"

#define encJ5Interrupt 3    //Interrupt pin number
#define enc1PinA      20   //Arduino pin number corresponding to interrupt pin
#define enc1PinB      30   //Digital pin number // Do not use pins 0, 1, 3, 8, 9, 11, 12, 13
#define MotorA        12
#define BrakeA        9
#define MotorApwn     3

//Pot & gripper pot pins
int J1 = A0;
int J2 = A1;
int J3 = A2;
int J4 = A3;
int G = A5;
//Pot & gripper analog values
double Base = 0;
double Shoulder = 0;
double Elbow = 0;
double Pitch = 0;
double Gripper = 0;
//input character
char input = 0;
//Base Pins
int InA1 = 22;
int InB1 = 23;
int PWM1 = 12;
//Shoulder Pins
int InA2 = 24;
int InB2 = 25;
int PWM2 = 11;
//Elbow Pins
int InB3 = 26;
int InA3 = 27;
int PWM3 = 10;
//Wrist Pitch Pins
int InB4 = 28;
int InA4 = 29;
int PWM4 = 9;
//Wrist Rotation Pins
int InB5 = 30;
int InA5 = 31;
int PWM5 = 8;
//Gripper Pins
int InB6 = 32;
int InA6 = 33;
int PWM6 = 7;
//Solenoid Pin (40)
int Sole = 53;
bool SoleState;
void setup() {
  Serial.begin(9600);
  pinMode(InA1, OUTPUT);
  pinMode(InB1, OUTPUT);
  pinMode(PWM1, OUTPUT);
  pinMode(InA2, OUTPUT);
  pinMode(InB2, OUTPUT);
  pinMode(PWM2, OUTPUT);
  pinMode(InA3, OUTPUT);
  pinMode(InB3, OUTPUT);
  pinMode(PWM3, OUTPUT);
  pinMode(InA5, OUTPUT);
  pinMode(InB5, OUTPUT);
  pinMode(PWM5, OUTPUT);
  pinMode(Sole, OUTPUT);

  //attachInterrupt(enc2Interrupt, encJ5ReadA, RISING); //Executes the function 'encJ5ReadA()' at rising edge of signal A from Encoder-1
  //analogWrite(MotorApwn, 0);

}

void loop() {

  //double encJ5Angle = (encJ5Ticks*2.0*pi/2048.0);
  //int A=digitalRead(A);
  

  if (Serial.available() > 0) {
    input = Serial.read();
    Serial.print("Received: ");
    Serial.println(input);

    if (input == 's') { //if statements for J2/shoulder
      Serial.println("J2 DOWN");
      digitalWrite(InA2, LOW);
      digitalWrite(InB2, HIGH);
      analogWrite(PWM2, 255);
    } else if (input == '2') {
      Serial.println("J2 UP");
      digitalWrite(InA2, HIGH);
      digitalWrite(InB2, LOW);
      analogWrite(PWM2, 255);
    } else if (input == 'w') {
      Serial.println("J2 STOP");
      digitalWrite(InA2, LOW);
      digitalWrite(InB2, LOW);
      analogWrite(PWM2, 0);

    } else if (input == 'd') { //if statements for J3/Elbow
      Serial.println("J3 DOWN");
      digitalWrite(InA3, HIGH);
      digitalWrite(InB3, LOW);
      analogWrite(PWM3, 60);
    } else if (input == '3') {
      Serial.println("J3 UP");
      digitalWrite(InA3, LOW);
      digitalWrite(InB3, HIGH);
      analogWrite(PWM3, 100);
    } else if (input == 'e') {
      Serial.println("J3 STOP");
      digitalWrite(InA3, LOW);
      digitalWrite(InB3, LOW);
      analogWrite(PWM3, 0);
      
    } else if (input == 'f') { //if statements for J4/Wrist Pitch
      Serial.println("J4 DOWN");
      digitalWrite(InA4, LOW);
      digitalWrite(InB4, HIGH);
      analogWrite(PWM4, 255);
    } else if (input == '4') {
      Serial.println("J4 UP");
      digitalWrite(InA4, HIGH);
      digitalWrite(InB4, LOW);
      analogWrite(PWM4, 255);
    } else if (input == 'r') {
      Serial.println("J4 STOP");
      digitalWrite(InA4, LOW);
      digitalWrite(InB4, LOW);
      analogWrite(PWM4, 0);

    } else if (input == '5') { //if statements for J5/Wrist Rotation
      Serial.println("J5 UP");
      digitalWrite(InA5, LOW);
      digitalWrite(InB5, HIGH);
      analogWrite(PWM5, 25);
    } else if (input == 'g') {
      Serial.println("J5 DOWN");
      digitalWrite(InA5, HIGH);
      digitalWrite(InB5, LOW);
      analogWrite(PWM5, 25);
    } else if (input == 't') {
      Serial.println("J5 STOP");
      digitalWrite(InA5, LOW);
      digitalWrite(InB5, LOW);
      analogWrite(PWM5, 0);

    } else if (input == 'a') { //if statements for J1/Base
      Serial.println("J1 DOWN");
      digitalWrite(InA1, HIGH);
      digitalWrite(InB1, LOW);
      analogWrite(PWM1, 80);
    } else if (input == '1') {
      Serial.println("J1 UP");
      digitalWrite(InA1, LOW);
      digitalWrite(InB1, HIGH);
      analogWrite(PWM1, 80);
    } else if (input == 'q') {
      Serial.println("J1 STOP");
      digitalWrite(InA1, LOW);
      digitalWrite(InB1, LOW);
      analogWrite(PWM1, 0);
    } else if (input == 'h') { //if statements for Gripper
      Serial.println("Gripper Close");
      digitalWrite(InA6, HIGH);
      digitalWrite(InB6, LOW);
      analogWrite(PWM6, 127);
    } else if (input == '6') {
      Serial.println("Gripper Open");
      digitalWrite(InA6, LOW);
      digitalWrite(InB6, HIGH);
      analogWrite(PWM6, 127);
    } else if (input == 'y') {
      Serial.println("Gripper STOP");
      digitalWrite(InA6, LOW);
      digitalWrite(InB6, LOW);
      analogWrite(PWM6, 0);

    }else if (input == '7') { //if statements for Solenoid
      Serial.println("Sole HIGH");
      digitalWrite(Sole, HIGH);
      SoleState=true;
    } else if (input == 'u') {
      Serial.println("Sole LOW");
      digitalWrite(Sole, LOW);
      SoleState=false;
    }
  }

  Base = analogRead(J1);
  Shoulder = analogRead(J2);
  Elbow = analogRead(J3);
  Pitch = analogRead(J4);
  Gripper = analogRead(G);
  //Mapped values of pot to degrees
  Base = (float)(map(Base, 0, 1024, 0, 3300) / 10.0)-183.0;
  Shoulder =(330-(float)((map(Shoulder, 0, 1024, 0, 3300) / 10.0 )))-173.5;
  Elbow = (float)(map(Elbow, 0, 1024, 0, 3300) / 10.0)-143.7;
  Pitch = (float)(map(Pitch, 0, 1024, 0, 3300) / 10.0)-166.6;
 
  Gripper = -(map(Gripper, 0, 1024, 0, 330)-171);
  Serial.print("J1: ");
  Serial.print(Base);
  Serial.print(" | J2: ");
  Serial.print(Shoulder);
  Serial.print(" | J3: ");
  Serial.print(Elbow);
  Serial.print(" | J4: ");
  Serial.print(Pitch);
  Serial.print(" | G: ");
  Serial.print(Gripper);
  //Serial.print(" | Sole: ");
  //Serial.println(SoleState);
  Serial.println();
  delay(100);
}
/*
void encJ5ReadA()
{
  if(!readPinFast(encJ5PinB))
  enc1Ticks++;
  else
  enc1Ticks--;
}*/
