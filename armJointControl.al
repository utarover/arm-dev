%autoZ on %makes temp variables

%Location Description
Newtonian N
bodies A,AA,B,BB,C,CC,D,DD,F,FF
points O,AB,BC,CD,DF,E
variables q{5}',u{5}'

constants La1x, La1y, La1z, La2x, La2y, La2z, La3x, La3y, La3z
constants Lb1x, Lb1y, Lb1z, Lb2x, Lb2y, Lb2z, Lb3x, Lb3y, Lb3z, Lb4x, Lb4y, Lb4z
constants Lc1x, Lc1y, Lc1z, Lc2x, Lc2y, Lc2z, Lc3x, Lc3y, Lc3z
constants Ld1x, Ld1y, Ld1z, Ld2x, Ld2y, Ld2z, Ld3x, Ld3y, Ld3z
constants Lf1x, Lf1y, Lf1z, Lf2x, Lf2y, Lf2z, Lf3x, Lf3y, Lf3z
constants GRA,GRB,GRC,GRD,GRF
constants grav
specified tau1,tau2,tau3,tau4,tau5

%zee_not = [grav, tau1,tau2,tau3, tau4, tau5] % do not put the folloing into temp variables

q1'=u1
q2'=u2
q3'=u3
q4'=u4
q5'=u5

GRA=1/(51*30); GRB=1/(263.728*40); GRC=1/(263.728*27); GRD=1/(263.728*27); GRF=1/(100*3);

simprot(N,A,2,q1)
simprot(N,AA,1,GRA*q1)
simprot(A,B,3,q2)
simprot(A,BB,1,GRB*q2)
simprot(B,C,3,q3)
simprot(B,CC,1,GRC*q3)
simprot(C,D,3,q4)
simprot(C,DD,1,GRD*q4)
simprot(D,F,1,q5)
simprot(D,FF,1,GRF*q5)

La1x=0.00523878; La1y=0.0510455; La1z=0.00591081;
La2x=0.13353695; La2y=0.03416269; La2z=0.04140198;
La3x; La3y=0.0436457; La3z;

Lb1x=0.09528819; Lb1y=0.02826982;Lb1y2=0.03967518; Lb1z;
Lb2x=0.3408328; Lb2y=0.00218397;Lb2z;
Lb3x=0.1671672; Lb3y=0; Lb3z=0.0944235;
Lb4x=0.07941319; Lb4y=0.01905; Lb4z;

Lc1x=0.22815086; Lc1y=0.0025034; Lc1z;
Lc2x=0.07940049; Lc2y=0.01270001; Lc2z;
Lc3x=0.12744914; Lc3y=0.00635; Lc3z;

Ld1x=0.06978059; Ld1y; Ld1z;
Ld2x=0.00596119; Ld2y=0.01216022; Ld2z;
Ld3x=0; Ld3y=0.04534263; Ld3z;

Lf1x=0.09074269; Lf1y=0.00389264; Lf1z;
Lf2x=0.11385116; Lf2y; Lf2z;

P_NO_O> = 0>
P_O_Ao>=(La1y)*N2>-(La1x)*N1> + (La1z)*N3>
P_O_AB>= La1y*N2>+La3y*N3> 
P_O_AAo> = (La2y)*N1>- (La2x)*N2>+(La2z)*N3>

P_AB_Bo>= (Lb1y)*A2>+(Lb1y2)*A2>+(Lb2x)*B1>+(Lb2y)*B2> %%add z
P_AB_BC>= (Lb1y)*A2>+(Lb1y2)*A2>+(Lb2x)*B1>+(Lb3x)*B2>
P_AB_BBo> = (Lb1y)*A2> - (Lb1x)*A1>

P_BC_Co>=-Lb3z*B3>+Lc1x*C1>+Lc1y*C2>
P_BC_CD>=-Lb3z*B3>+Lc1x*C1>+Lc3x*C1>-Lc3y*C2>
P_BC_CCo>=Lb4y*B2>-Lb4x*B1>

P_CD_Do>=Lb3z*D3>-Ld2x*D1>+Ld2y*D2>
P_CD_DF>=Lb3z*D3>+Ld1x*D1>
P_CD_DDo>=Lc3y*C2>+Lc2y*C2>-Lc2x*C1>

P_DF_Fo>=Lf1x*F1>+Lf1y*F2>
P_DF_E>=Lf1x*F1>+Lf2x*F1>
P_DF_FFo> = -Ld1x*D1> + Ld3y*D2>+Ld3x*D1>

%express(P_No_E>,N)
%explicit(P_No_E>)
%evaluate(express(P_No_E>,N),q1=0,q2=0,q3=0,q4=0,q5=0)
%evaluate(express(P_No_BC>,N),q1=0,q2=0,q3=0,q4=0,q5=0)
%evaluate(express(P_No_CD>,N),q1=0,q2=0,q3=0,q4=0,q5=0)


%Velocity
W_A_N>=u1*A2>
W_AA_N>=GRA*u1*N1>

W_B_A>=u2*B3>
W_BB_A>=GRB*u2*A1>

W_C_B>=u3*C3>
W_CC_B>=GRC*u3*B1>

W_D_C>=u4*D3>
W_DD_C>=GRD*u4*C1>

W_F_D>=u5*F1>
W_FF_D>=-GRF*u5*D1>

V_Ao_N>=dt(P_O_Ao>,N)
V_AAo_N>=dt(P_O_AAo>,N)
V_Bo_N>=dt(P_O_Bo>,N)
V_BBo_N>=dt(P_O_BBo>,N)
V_Co_N>=dt(P_O_Co>,N)
V_CCo_N>=dt(P_O_CCo>,N)
V_Do_N>=dt(P_O_Do>,N)
V_DDo_N>=dt(P_O_DDo>,N)
V_Fo_N>=dt(P_O_Fo>,N)
V_FFo_N>=dt(P_O_FFo>,N)
V_E_N>=dt(P_O_E>,N)

%acceleration
ALF_A_N> = dt(W_A_N>,N)
ALF_AA_N> = dt(W_AA_N>,N)
ALF_B_N> = dt(W_B_N>,N)
ALF_BB_N> = dt(W_BB_N>,N)
ALF_C_N> = dt(W_C_N>,N)
ALF_CC_N> = dt(W_CC_N>,N)
ALF_D_N> = dt(W_D_N>,N)
ALF_DD_N> = dt(W_DD_N>,N)
ALF_F_N> = dt(W_F_N>,N)
ALF_FF_N> = dt(W_FF_N>,N)
A_Ao_N> = dt(V_Ao_N>,N)
A_AAo_N> = dt(V_AAo_N>,N)
A_Bo_N> = dt(V_Bo_N>,N)
A_BBo_N> = dt(V_BBo_N>,N)
A_Co_N> = dt(V_Co_N>,N)
A_CCo_N> = dt(V_CCo_N>,N)
A_Do_N> = dt(V_Do_N>,N)
A_DDo_N> = dt(V_DDo_N>,N)
A_Fo_N> = dt(V_Fo_N>,N)
A_FFo_N> = dt(V_FFo_N>,N)

%Mass properties
mass A=mA, B=mB, C=mC, D=mD, F=mF, AA=mAA, BB=mBB, CC=mCC, DD=mDD, FF=mFF
inertia A,IA1,IA2,IA3,0,0,0
IA1 = 0.01; IA2 = 0.01; IA3=0.01

inertia AA,IAA1,IAA2,IAA3,0,0,0
IAA1 = 0.0001; IAA2 = 0.0001; IAA3=0

inertia B,IB1,IB2,IB3,0,0,0
IB1 = 0; IB2 = 0.02; IB3=0.02

inertia BB,IBB1,IBB2,IBB3,0,0,0
IBB1 = 0.0001; IBB2 = 0; IBB3=0.0001

inertia C,IC1,IC2,IC3,0,0,0
IC1 = 0; IC2 = 0.011; IC3=0.011

inertia CC,ICC1,ICC2,ICC3,0,0,0
ICC1 = 0; ICC2 = 0.001; ICC3=0.001

inertia D,ID1,ID2,ID3,0,0,0
ID1 = 0.0004; ID2 = 0.0017; ID3=0.0019

inertia DD,IDD1,IDD2,IDD3,0,0,0
IDD1 = 0; IDD2 = 0.0003; IDD3=0.0003

inertia F,IF1,IF2,IF3,0,0,0
IF1 = 0; IF2 = 0.02; IF3=0.02

inertia FF,IFF1,IFF2,IFF3,0,0,0
IFF1 = 0.0001; IFF2 = 0.0001; IFF3=0.0002


%Forces and Moments
gravity (-grav*N2>)

torque(AA/N,-tau1*A1>)
torque(BB/A, -tau2*B1>)
torque(CC/B, -tau3*C1>)
torque(DD/C, -tau4*D1>)
torque(FF/D, -tau5*F1>)

%Equations of motion
ZERO = FR() + FRSTAR()
%Ieff=coef(zero,[u1,u2,u3,u4,u5])/coef(zero,[tau1, tau2,tau3,tau4,tau5])

A=-coef(ZERO,[u1',u2',u3',u4',u5'])
B=-exclude(ZERO,[u1',u2',u3',u4',u5',tau1,tau2,tau3,tau4,tau5,grav])
g = grav*coef(ZERO,grav)
gear = coef(ZERO,[tau1,tau2,tau3,tau4,tau5])

%JJ = [dot(V_E_N>,N1>);dot(V_E_N>,N2>);dot(V_E_N>,N3>);dot(W_D_C>,A3>);dot(W_F_D>,D1>)]
%J=explicit(coef(JJ,[u1,u2,u3,u4,u5]))
%Jinv = inverse(J)
%Jdqd = dt(J)*[u1;u2;u3;u4;u5]

%specified xd,xdd,xddd
%specified yd,ydd,yddd
%specified zd,zdd,zddd
%specified q4d,q4dd,q4ddd
%specified q5d,q5dd,q5ddd

specified q1d,q1dd,q1ddd
specified q2d,q2dd,q2ddd
specified q3d,q3dd,q3ddd
specified q4d,q4dd,q4ddd
specified q5d,q5dd,q5ddd

constants kp,kv

taustar = [q1ddd,q2ddd,q3ddd,q4ddd,q5ddd] + 2*sqrt(kp)*([q1dd,q2dd,q3dd,q4dd,q5dd]-[u1,u2,u3,u4,u5]) + kp*([q1d,q2d,q3d,q4d,q5d]-[q1,q2,q3,q4,q5])
tauu=inverse(gear)*(A*transpose(taustar) + b - g)
%taustar = [xddd;yddd;zddd;q4ddd;q5ddd] + 2*sqrt(kp)*([xdd;ydd;zdd;q4dd;q5dd]-[dot(V_E_N>,N1>);dot(V_E_N>,N2>);dot(V_E_N>,N3>);u4;u5]) + kp*([xd;yd;zd;q4d;q5d]-[dot(P_No_E>,N1>);dot(P_No_E>,N2>);dot(P_No_E>,N3>);q4;q5])
%tauu=inverse(gear)*(A*Jinv*(taustar-Jdqd)+b-g)

tau1 = tauu[1]
tau2 = tauu[2]
tau3 = tauu[3]
tau4 = tauu[4]
tau5 = tauu[5]
% 0.98 0.6579 0.479
input mA=2.67,mAA=0.37, mB=0.98,mBB=0.37, mC=0.693,mCC=0.37, mD=0.447,mDD=0.37,mF=5.72,mFF=0.37, grav = 9.81, kp=1
output t,q1,q2,q3,q4,q5,u1,u2,u3,u4,u5
output t, tau1, tau2, tau3,tau4,tau5, u1,u2,u3,u4,u5
%code dynamics() armtestJointSpace.c